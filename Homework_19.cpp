﻿#include <iostream>

class Animal {
public:
    virtual void Voice() const {
        std::cout << "Some animal sound\n";
    }

};

class Dog : public Animal {
public:
    void Voice() const override final {
        std::cout << "Dog goes woof\n";
    }
};

class Cat : public Animal {
public:
    void Voice() const override final {
        std::cout << "Cat goes meow\n";
    }
};

class Fox : public Animal {
public:
    void Voice() const override final {
        std::cout << "But there's one sound\nThat no one knows\nWhat does the fox say?\n";
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Fox();
    for (auto animal : animals)
        animal->Voice();
}
